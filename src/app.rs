use leptos::*;
use leptos_router::{Route, Router, Routes};

use crate::components::viewer::*;
use crate::routes::{
    config::Config, markdown_html_viewer::MarkdownHtmlViewer, topbar::Topbar, user::User,
};

#[component]
pub fn App() -> impl IntoView {
    provide_context(MarkdownHtmlForView::default());
    view! {
       <Router>
        <div class="h-screen w-full">
            <Topbar/>
                <Routes>
                        <Route path="/" view=MarkdownHtmlViewer/>
                        <Route path="/config" view=Config/>
                        <Route path="/user" view=User/>
                </Routes>
        </div>
        </Router>
    }
}
