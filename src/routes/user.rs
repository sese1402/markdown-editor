use leptos::*;

#[component]
pub fn User() -> impl IntoView {
    view! { <p>user</p>}
}
