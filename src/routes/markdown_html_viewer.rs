use crate::components::viewer::MarkdownHtmlForView;
use leptos::{html::Div, *};
use leptos_use::{use_scroll_with_options, UseScrollOptions, UseScrollReturn};

#[component]
pub fn MarkdownHtmlViewer() -> impl IntoView {
    let markdown_html_for_view = use_context::<MarkdownHtmlForView>().unwrap();
    let el = create_node_ref::<Div>();
    let UseScrollReturn {
        x, y, set_x, set_y, ..
    } = use_scroll_with_options(el, UseScrollOptions::default());

    view! {
        <div class="flex flex-row w-full h-full">
            <div class="pr-5 mt-20 basis-1/2 h-full border-2 bg-gray-700 border-gray-900 overflow-scroll">
                <article class="w-full h-full prose prose-neutral text-white" inner_html={
                    let markdown_html_for_view_clone = markdown_html_for_view.clone();
                     move || markdown_html_for_view_clone.html.get()}>
                </article>
                </div>
                <form
                    id="text_input"
                    class="basis-1/2 h-full"
                    on:input = {
                let markdown_html_for_view_clone = markdown_html_for_view.clone();
                move |event| markdown_html_for_view_clone.set_after_write_in_textarea(event)}>
                    <textarea
                        type="text"
                        name="txtarea"
                        class="pr-5 mt-20 h-full w-full bg-gray-700 border-2 border-gray-900 bg-gray-700"
                        inner_html={move || markdown_html_for_view.md.get()}
                    ></textarea>
                </form>
            </div>
    }
}
