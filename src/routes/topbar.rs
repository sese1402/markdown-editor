use gloo_file::FileList;
use leptos::*;

use crate::components::viewer::MarkdownHtmlForView;

#[component]
pub fn Topbar() -> impl IntoView {
    let markdown_html_for_view = use_context::<MarkdownHtmlForView>().unwrap();
    let input_ref = create_node_ref::<html::Input>();
    let (aktiv_file_name, aktiv_file_name_set) = create_signal(String::from("open new file"));

    let on_file_addition = move |files: FileList| {
        let file = files[0].clone();
        aktiv_file_name_set.set(file.name());
        let future = async move {
            let text = gloo_file::futures::read_as_text(&file)
                .await
                .expect("filereaderror");
            markdown_html_for_view.set_after_open_new_file(text)
        };
        spawn_local(future);
    };

    let on_change = move |_| {
        if let Some(input_ref) = input_ref.get_untracked() {
            if let Some(files) = input_ref.files() {
                on_file_addition(FileList::from(files));
            }
        }
    };

    view! {
        <div class="flex justify-between w-full fixed bg-gray-800 ">
            <p>test</p>
            <label>
            <p inner_html={aktiv_file_name.get()}></p>
            <input class="hidden h-full center" ref=input_ref type="file" on:change=on_change/>
            </label>
            <p>test2</p>
        </div>
    }
}
