use leptos::*;
use CorrosionMark::md_to_html;

#[derive(Clone, Copy)]
pub struct MarkdownHtmlForView {
    pub md: ReadSignal<String>,
    pub md_set: WriteSignal<String>,
    pub html: ReadSignal<String>,
    pub html_set: WriteSignal<String>,
}

impl MarkdownHtmlForView {
    pub fn set_after_write_in_textarea(&self, event: ev::Event) {
        self.md_set.set(event_target_value(&event));
        self.html_set.set(md_to_html(self.md.get().as_str()).unwrap());
    }

    pub fn set_after_open_new_file(&self, file: String) {
        self.md_set.set(file);
        self.html_set.set(md_to_html(self.md.get().as_str()).unwrap());
    }
}

impl Default for MarkdownHtmlForView {
    fn default() -> Self {
        let md_signals = create_signal(String::new());
        let html_signals = create_signal(String::new());
        Self {
            md: md_signals.0,
            md_set: md_signals.1,
            html: html_signals.0,
            html_set: html_signals.1,
        }
    }
}
