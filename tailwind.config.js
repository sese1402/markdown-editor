/** @type {import('tailwindcss').Config} */
    module.exports = {
      content: {
        relative: true,
        files: ["index.html", "*.html", "./src/**/*.rs"],
      },
      theme: {
        extend: {
        },
      },
      plugins: [
        require("@tailwindcss/typography"),
        require('flowbite/plugin'),
        require("tailwind-scrollbar")
      ],
    }
    
